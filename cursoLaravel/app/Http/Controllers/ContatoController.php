<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contato;

class ContatoController extends Controller
{
    public function index()
    {
        $contatos = [
            (object)["nome" => "Joao", "tel" => "500"],
            (object)["nome" => "Maria", "tel" => "700"]
        ];
        $contato = new Contato();
        dd($contato->lista());
        return view('contato.index', compact('contatos'));
    }

    public function criar(Request $req){
        dd($req->all());
        return "Esse é o criar de ContatoController";
    }
    public function editar(){
        return "Esse é o editar de ContatoController";
    }
}
