@extends('layout.site')

@section('titulo','Cursos')

@section('conteudo')
    <div class="container">
        <h3 class="container">Adicionar Curso</h3>
        <form class="" action="{{route('admin.atualizar',$registro->id)}} " method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            @include('admin.cursos._form')
            <button class="btn blue">Atualizar</button>
        </form>
    </div>
@endsection