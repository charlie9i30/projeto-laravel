@extends('layout.site')

@section('titulo','Cursos')

@section('conteudo')
    <div class="container">
        <h3 class="container">Editar Curso</h3>
        <form class="" action="{{route('admin.salvar')}} " method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @include('admin.cursos._form')
            <button class="btn blue">Salvar</button>
        </form>
    </div>
@endsection