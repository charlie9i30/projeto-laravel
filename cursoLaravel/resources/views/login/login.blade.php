@extends('layout.site')

@section('titulo','Login')

@section('conteudo')
    <div class="container">
        <h3 class="container">Entrar</h3>
        <form class="" action="{{route('site.entrar')}}" method="post" >
            {{csrf_field()}}
            <div class="input-field">
                <input type="text" name="email">
                <label>Email</label>
            </div>
            <div class="input-field">
                <input type="password" name="senha">
                <label>Senha</label>
            </div>
            <button class="btn blue">Entrar</button>
        </form>
    </div>
@endsection