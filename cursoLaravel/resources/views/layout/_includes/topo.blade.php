<!DOCTYPE html>
<html>
<head>
    <title> @yield('titulo')</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>

<header>
    <nav>
        <div class="nav-wrapper deep-orange">
            <div class="container">
                <a href="/" class="brand-logo">Cursos</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="/">Home</a></li>
            @if(Auth::guest())
                <li><a href="{{route('site.login')}}">Login</a></li>
            @else
                <li><a href="{{route('admin.curso')}}">Cursos</a></li>
                <li><a href="#">{{Auth::user()->name}}</a></li>
                <li><a href="{{route('site.sair')}}">Sair</a></li>
            @endif
            </ul>
            </div>
        </div>
    </nav>
</header>


