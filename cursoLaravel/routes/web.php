<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/',['as'=>'site.home','uses'=>'Site\SiteController@index']);
Route::get('/login',['as'=>'site.login','uses'=>'Site\LoginController@login']);
Route::post('/login/entrar',['as'=>'site.entrar','uses'=>'Site\LoginController@entrar']);
Route::get('/login/sair',['as'=>'site.sair','uses'=>'Site\LoginController@sair']);

Route::group(['middleware'=>'auth'],function(){
    Route::get('/admin/curso',['as'=>'admin.curso','uses'=>'Admin\CursoController@index']);
    Route::get('/admin/curso/adicionar',['as'=>'admin.adicionar','uses'=>'Admin\CursoController@adicionar']);
    Route::post('/admin/curso/salvar',['as'=>'admin.salvar','uses'=>'Admin\CursoController@salvar']);
    Route::get('/admin/curso/editar/{id}',['as'=>'admin.editar','uses'=>'Admin\CursoController@editar']);
    Route::put('/admin/curso/atualizar/{id}',['as'=>'admin.atualizar','uses'=>'Admin\CursoController@atualizar']);
    Route::get('/admin/curso/deletar/{id}',['as'=>'admin.deletar','uses'=>'Admin\CursoController@deletar']);
});


